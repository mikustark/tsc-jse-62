package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.model.Project;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, String> {
    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull List<Project> findAllByUserId(@Nullable String userId);

    @Nullable Project findByIdUserId(@Nullable String userId, @Nullable String id);

    void remove(@NotNull Project entity);

    void removeById(@Nullable String id);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);
}
