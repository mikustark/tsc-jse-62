package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyLoginOrPasswordException extends AbstractException {
    public EmptyLoginOrPasswordException() {
        super("Error Login or Password.");
    }

    public EmptyLoginOrPasswordException(String value) {
        super("Error Login or Password. " + value);
    }
}
