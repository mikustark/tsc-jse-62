package ru.tsc.karbainova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

public class UserUnlockByLoginListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "User unlock";
    }

    @Override
    @EventListener(condition = "@userUnlockByLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        adminEndpoint.unlockUserByLoginUser(sessionService.getSession(), login);
    }

}
